package firebase

import (
	"context"
	"errors"
	"fmt"
	"log"

	"cloud.google.com/go/firestore"
	firebase "firebase.google.com/go"
	"firebase.google.com/go/auth"
	general "gitlab.com/viggy28/ageneral-go"
)

// User object
type User struct {
	UID          string   `json:"uid"`
	AirtableID   string   `json:"airtableid"`
	FirstName    string   `json:"firstName"`
	LastName     string   `json:"lastName"`
	DisplayName  string   `json:"displayName"`
	Email        string   `json:"email"`
	Password     string   `json:"password"`
	PhoneNo      string   `json:"phoneNo"`
	Status       string   `json:"status"`
	Location     Location `json:"location"`
	ZipCode      int      `json:"zipCode"`
	IGUserName   string   `json:"igUserName"`
	UserMetadata struct {
		CreationTimestamp int64
	}
}

// Location to store the location of the user
type Location struct {
	City  string `json:"city"`
	State string `json:"state"`
}

type FirebaseApp struct {
	Fb *firebase.App
}

// Initialize does all the initialization work
// Add conf and opt argument for NewApp()
func Initialize(ctx context.Context) (FirebaseApp, error) {
	var app FirebaseApp
	var err error
	app.Fb, err = firebase.NewApp(ctx, nil)
	if err != nil {
		return app, err
	}
	return app, nil
}

// TODO displayName seems hardcoded
// FirestoreCreateRecord creates a record with the specificed id, collection & data
func (app *FirebaseApp) FirestoreCreateRecord(id, collection string, data map[string]interface{}) error {
	ctx := context.Background()
	firestoreClient, err := app.Fb.Firestore(ctx)
	general.Check(err, true, "Unable to create a New Firestore client")
	_, err = firestoreClient.Collection(collection).Doc(id).Create(ctx, data)
	if err != nil {
		displayName, ok := data["displayName"].(string)
		if !ok {
			log.Println("ERROR: displayName is not a string")
		}
		log.Println("Error creating a document in firestore for user "+displayName+" with UID "+id+" because of error", err)
		return fmt.Errorf("Internal Error. Please reach out to customer@aprl.la")
	}
	return nil
}

// FirestoreCreateRecordWithoutID creates a record with the specificed id, collection & data
func (app *FirebaseApp) FirestoreCreateRecordWithoutID(collection string, data map[string]interface{}) (*firestore.DocumentRef, error) {
	ctx := context.Background()
	firestoreClient, err := app.Fb.Firestore(ctx)
	general.Check(err, true, "Unable to create a New Firestore client")
	docRef, writeResult, err := firestoreClient.Collection(collection).Add(ctx, data)
	if err != nil {
		log.Println("Error creating a document in firestore for at time ", writeResult.UpdateTime, err)
		return &firestore.DocumentRef{}, fmt.Errorf("Internal Error. Please reach out to customer@aprl.la")
	}
	log.Println("Created a new document in Firestore with ID:", docRef.ID, " in collection:", collection)
	return docRef, nil
}

// FirestoreReadRecord reads a record with the specificed id, collection & returns the record in a map
func (app *FirebaseApp) FirestoreReadRecord(collectionName string, ID string) (map[string]interface{}, error) {
	ctx := context.Background()
	firestoreClient, err := app.Fb.Firestore(ctx)
	general.Check(err, true, "Error creating a firestoreClient in firestoreReadRecord()")
	documentSnaphot, err := firestoreClient.Collection(collectionName).Doc(ID).Get(ctx)
	if err != nil {
		message := "ALERT: Error doing Get() on firestoreReadRecord for ID" + ID + " in collection" + collectionName
		log.Println(message, err)
		return nil, err
	}
	data := documentSnaphot.Data()
	return data, nil
}

// FirestoreUpdateRecord updates a record with the specificed id, collection & returns the record in a maps
func (app *FirebaseApp) FirestoreUpdateRecord(collection, id string, data map[string]interface{}, ch *chan error) {
	var field, value interface{}
	for field, value = range data {
	}
	ctx := context.Background()
	firestoreClient, err := app.Fb.Firestore(ctx)
	general.Check(err, true, "Error creating a firestoreClient in firestoreUpdateRecord()")
	_, err = firestoreClient.Collection(collection).Doc(id).Set(ctx, data, firestore.MergeAll)
	if err != nil {
		message := "ALERT: Error doing Set() on firestoreUpdateRecord() for UID" + id + " in collection" + collection
		log.Println(message, err)
		*ch <- errors.New("Internal Error. Please reach out to customer@aprl.la")
		return
	}
	log.Println("INFO: Updated collection:" + collection + "\nid: " + id + "\nfield: " + field.(string) + "\nvalue: " + value.(string))
	*ch <- nil
}

// AuthUpdate updates a record with the specificed uid
func (app *FirebaseApp) AuthUpdate(UID string, userToUpdate *auth.UserToUpdate, ch *chan error) {
	ctx := context.Background()
	firebaseAuthClient, err := app.Fb.Auth(ctx)
	if err != nil {
		log.Fatalf("Error in initializing firebase App %v", err)
		*ch <- err
		return
	}
	ur, err := firebaseAuthClient.UpdateUser(ctx, UID, userToUpdate)
	if err != nil {
		message := "ALERT: Error doing UpdateUser() on firebaseAuthUpdate() for UID" + UID
		log.Println(message, err)
		*ch <- errors.New("Internal Error. Please reach out to customer@aprl.la")
		return
	}
	log.Println("INFO: Updated user in firebase auth:", ur.UID, " display name:", ur.DisplayName)
	*ch <- nil
}

// FirestoreUpdateUserStatus updates the user status in firestore and firebase auth. TODO remove the slack messages and add them in caller
func (app *FirebaseApp) FirestoreUpdateUserStatus(collectionName, field, value string, UID string) error {
	var disabled bool
	switch {
	case value == "submitted":
		disabled = true
	case value == "waitlisted":
		disabled = true
	case value == "accepted":
		disabled = false
	case value == "activated":
		disabled = false
	default:
		return errors.New("Error: Unaccepted user status value")
	}
	firestoreUpdateCh := make(chan error)
	firebaseAuthUpdateCh := make(chan error)
	var userToBeUpdated *auth.UserToUpdate
	userToBeUpdated = (&auth.UserToUpdate{}).Disabled(disabled)
	data := make(map[string]interface{})
	data[field] = value
	go app.FirestoreUpdateRecord(collectionName, UID, data, &firestoreUpdateCh)
	go app.AuthUpdate(UID, userToBeUpdated, &firebaseAuthUpdateCh)
	err1, err2 := <-firestoreUpdateCh, <-firebaseAuthUpdateCh
	if err1 != nil {
		message := "ALERT: Error updating a firestore record ID:" + UID + " in collection:" + collectionName
		log.Println(message, err1)
		return err1
	}
	if err2 != nil {
		message := "ALERT: Error updating firebase authentication for for UID:" + UID
		log.Println(message, err2)
		return err2
	}
	return nil
}

// Jwt2uid takes a jwt token & converts in into uid
// TODO this function name should firebase ID token to UID
// https://firebase.google.com/docs/auth/users#auth_tokens
func (app *FirebaseApp) Jwt2uid(JWTtoken string, chStr *chan string, chErr *chan error) {
	ctx := context.Background()
	authClient, err := app.Fb.Auth(ctx)
	if err != nil {
		log.Println("Error initializing firebase auth", err)
	}
	token, err := authClient.VerifyIDToken(ctx, JWTtoken)
	if err != nil {
		log.Println("ERROR: The JWT token verification is failed because of ", err)
		*chStr <- ""
		*chErr <- err
		return
	}
	*chStr <- token.UID
	*chErr <- nil
}

// UID2Info returns the user info for the given UID
func (app *FirebaseApp) UID2Info(uid string) (*auth.UserRecord, error) {
	ctx := context.Background()
	authClient, err := app.Fb.Auth(ctx)
	if err != nil {
		log.Println("Error initializing firebase auth", err)
	}
	userRecord, err := authClient.GetUser(ctx, uid)
	if err != nil {
		log.Println("Error getting user info", err)
		return nil, err
	}
	return userRecord, nil
}
