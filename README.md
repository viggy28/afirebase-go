# Go package to perform common firebase related operations

## How to use
``` 
go get gitlab.com/viggy28/afirebase-go
```
and import on your program

```
import
(
    afirebase gitlab.com/viggy28/afirebase-go
)
```

**Step1: Initialize**

The package is going to use Application Default Credentials.

If your app is running in Google Cloud Funtions, Google Kubernetes Engine, etc., then its going to use the default Service Account.

```
firebaseApp, err := afireabse.Initialize()
if err != nil {
    log.Fatalf("error initializing afirebase package:%v", err)
}
```

If your app is running any where other than Google Cloud services (eg. localhost, on-prem server, AWS EC2, etc.), then you have to first set the environment variable GOOGLE_APPLICATION_CREDENTIALS to a service account.
```
Eg: export GOOGLE_APPLICATION_CREDENTIALS="/home/user/Downloads/[FILE_NAME].json"
```

**Step2: Perform operations**

Call the methods using the firebaseApp type

```
data := make(map[string]interface{})
data["firstName"] = "ravi"
data["lastName"] = "chandran"
data["displayName"] = "kr"
err := firebaseApp.FirestoreCreateRecord("userid1", "userscollection", data)
if err != nil {
    log.Println("error creating record in firestore", err)
}
```
